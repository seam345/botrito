use nom::branch::{alt, permutation};
use nom::bytes::complete::{tag, take_till, take_while1};
use nom::character::complete::digit1;
use nom::combinator::{all_consuming, map_res, verify};
use nom::error::Error;
use nom::sequence::{preceded, separated_pair, tuple};
use nom::{Finish, IResult};

use anyhow::Result;
use std::str::FromStr;
use time::macros::offset;
use time::{OffsetDateTime, UtcOffset};

use super::types;

fn is_separator_char(c: char) -> bool {
    c == ' ' || c == ',' || c == ':'
}

fn separator(input: &str) -> IResult<&str, &str> {
    take_while1(is_separator_char)(input)
}

fn word(input: &str) -> IResult<&str, &str> {
    take_till(is_separator_char)(input)
}

fn train_name(input: &str) -> IResult<&str, &str> {
    preceded(separator, word)(input)
}

fn time_block(input: &str) -> IResult<&str, u8> {
    map_res(digit1, |s: &str| s.parse::<u8>())(input)
}

fn helper_time(hour: u8, min: u8) -> OffsetDateTime {
    let now = OffsetDateTime::now_utc().to_offset(offset!(+0));
    now.replace_hour(hour)
        .expect("hour should be between 0 and 24")
        .replace_minute(min)
        .expect("min should be between 0 and 60")
        .replace_second(0)
        .expect("second should be between 0 and 60")
        .replace_nanosecond(0)
        .expect("nano should be between 0 and 99")
        .to_offset(UtcOffset::UTC)
}

fn time(input: &str) -> IResult<&str, OffsetDateTime> {
    let hours = verify(time_block, |t: &u8| (0..24).contains(t));
    let mins = verify(time_block, |t: &u8| (0..60).contains(t));

    let (rem, (hour, min)) = preceded(separator, separated_pair(hours, tag(":"), mins))(input)?;
    Ok((rem, helper_time(hour, min)))
}

fn create(input: &str) -> IResult<&str, types::ControllerCommand> {
    let (rem, (_, train, departure)) = all_consuming(permutation((
        preceded(separator, tag("create")),
        train_name,
        time,
    )))(input)?;
    Ok((
        rem,
        types::ControllerCommand::Create {
            train: train.to_string(),
            departure,
        },
    ))
}

fn join(input: &str) -> IResult<&str, types::ControllerCommand> {
    let (rem, (_, train)) = all_consuming(tuple((
        preceded(separator, alt((tag("join"), tag("🎫"), tag("🎟️")))),
        train_name,
    )))(input)?;
    Ok((rem, types::ControllerCommand::Join(train.to_string())))
}

fn leave(input: &str) -> IResult<&str, types::ControllerCommand> {
    let (rem, (_, train)) =
        all_consuming(tuple((preceded(separator, tag("leave")), train_name)))(input)?;
    Ok((rem, types::ControllerCommand::Leave(train.to_string())))
}

fn command(input: &str) -> IResult<&str, types::ControllerCommand> {
    alt((create, join, leave))(input)
}

impl FromStr for types::ControllerCommand {
    type Err = Error<String>;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let (rem, _botname) = word(s).expect("Botname should be at the start of the commands");
        match command(rem).finish() {
            Ok(cmd) => Ok(cmd.1),
            Err(Error { input, code }) => Err(Error {
                input: input.to_string(),
                code,
            }),
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn separator_chars() {
        let separators = [' ', ',', ':'];
        assert!(separators.iter().all(|&c| is_separator_char(c)));
    }

    #[test]
    fn parse_separators() {
        assert_eq!(separator("    "), Ok(("", "    ")));
        assert_eq!(separator(",,,"), Ok(("", ",,,")));
        assert_eq!(separator("::"), Ok(("", "::")));
        assert_eq!(separator(" ,,:::"), Ok(("", " ,,:::")));
        assert_eq!(separator(" ,,:::blabla"), Ok(("blabla", " ,,:::")));
    }

    #[test]
    fn parse_word() {
        assert_eq!(
            word("botrito create, testtrain 22:00"),
            Ok((" create, testtrain 22:00", "botrito"))
        );
        assert_eq!(
            word("botrito:: create, testtrain 22:00"),
            Ok((":: create, testtrain 22:00", "botrito"))
        );
    }

    #[test]
    fn parse_train_name() {
        assert_eq!(train_name(" testtrain 22:00"), Ok((" 22:00", "testtrain")));
        assert_eq!(train_name(" testtrain 22:00"), Ok((" 22:00", "testtrain")));
        assert_eq!(train_name(" testtrain 22:00"), Ok((" 22:00", "testtrain")));
    }

    #[test]
    fn parse_time_block() {
        assert_eq!(time_block("1"), Ok(("", 1)));
        assert_eq!(time_block("12"), Ok(("", 12)));
        assert_eq!(time_block("1:1"), Ok((":1", 1)));
        assert_eq!(time_block("12:12"), Ok((":12", 12)));
    }

    #[test]
    fn parse_time() {
        assert_eq!(time(" 12:30"), Ok(("", helper_time(12, 30))));
        assert_eq!(time(" 1:30"), Ok(("", helper_time(1, 30))));
        assert_eq!(time(" 12:0"), Ok(("", helper_time(12, 0))));
        assert_eq!(time(" 1:0"), Ok(("", helper_time(1, 0))));
        assert_eq!(time(" 12:30foo"), Ok(("foo", helper_time(12, 30))));
    }

    #[test]
    fn parse_command() {
        assert_eq!(
            command(" create boritotrain 13:00"),
            Ok((
                "",
                types::ControllerCommand::Create {
                    train: "boritotrain".to_string(),
                    departure: helper_time(13, 0)
                }
            ))
        );

        assert_eq!(
            command(" join boritotrain"),
            Ok((
                "",
                types::ControllerCommand::Join("boritotrain".to_string())
            ))
        );
        assert_eq!(
            command(" 🎫 boritotrain"),
            Ok((
                "",
                types::ControllerCommand::Join("boritotrain".to_string())
            ))
        );
        assert_eq!(
            command(" 🎟️ boritotrain"),
            Ok((
                "",
                types::ControllerCommand::Join("boritotrain".to_string())
            ))
        );

        assert_eq!(
            command(" leave boritotrain"),
            Ok((
                "",
                types::ControllerCommand::Leave("boritotrain".to_string())
            ))
        );
    }

    #[test]
    fn parse_msg() {
        assert_eq!(
            "botrito: create party 18:00".parse::<types::ControllerCommand>(),
            Ok(types::ControllerCommand::Create {
                train: "party".to_string(),
                departure: helper_time(18, 0)
            })
        );
        assert_eq!(
            "botrito create burritotrain 12:30".parse::<types::ControllerCommand>(),
            Ok(types::ControllerCommand::Create {
                train: "burritotrain".to_string(),
                departure: helper_time(12, 30)
            })
        );

        assert_eq!(
            "botrito join burritotrain".parse::<types::ControllerCommand>(),
            Ok(types::ControllerCommand::Join("burritotrain".to_string()))
        );
        assert_eq!(
            "botrito 🎟️ burritotrain".parse::<types::ControllerCommand>(),
            Ok(types::ControllerCommand::Join("burritotrain".to_string()))
        );
        assert_eq!(
            "botrito join party".parse::<types::ControllerCommand>(),
            Ok(types::ControllerCommand::Join("party".to_string()))
        );

        assert_eq!(
            "botrito leave burritotrain".parse::<types::ControllerCommand>(),
            Ok(types::ControllerCommand::Leave("burritotrain".to_string()))
        );
        assert_eq!(
            "botrito leave party".parse::<types::ControllerCommand>(),
            Ok(types::ControllerCommand::Leave("party".to_string()))
        );
    }
}
