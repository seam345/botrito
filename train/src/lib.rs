use anyhow::{bail, Context, Result};
use time::OffsetDateTime;
use tokio::sync::{mpsc, oneshot};
use tracing::{debug, error, info, warn};

use std::fmt;

mod parser;
mod train;
mod types;

pub enum BotError {
    Unknown,
    InvalidCommand,
    UnknownCommand,
}

impl fmt::Display for BotError {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        match self {
            BotError::InvalidCommand => write!(f, "Invalid command"),
            BotError::UnknownCommand => write!(f, "Unknow command. Try "),
            BotError::Unknown => write!(f, "Something went wrong try again ヽ(。_°)ノ"),
        }
    }
}

pub trait Bot {
    fn handle_message(
        &self,
        sender: String,
        msg: String,
        transmitter: mpsc::Sender<String>,
    ) -> Result<String>;
}

#[derive(Default)]
pub struct Controller {
    timetable: types::Timetable,
}

impl Controller {
    fn create_train(
        &self,
        name: String,
        departure: OffsetDateTime,
        irc_chan: mpsc::Sender<String>,
    ) -> Result<()> {
        if departure <= OffsetDateTime::now_utc() {
            warn!("Cannot create Trains in the past");
            bail!("Cannot create Trains in the past");
        }
        if let Some(train) = self.timetable.schedule_train(&name, departure) {
            tokio::spawn(train::run_train(train, self.timetable.clone(), irc_chan));
            info!("Train {} started", &name);
            Ok(())
        } else {
            bail!(
                "Failed to create Train {}, a Train with a similar name already exist",
                &name
            );
        }
    }

    fn join_train(&self, train: String, attendee: String) -> Result<()> {
        info!("{} is trying to join Train {}", attendee, train);
        if let Some(transmitter) = self.timetable.get_train_transmitter(&train) {
            debug!("Send AttendeeJoin command to Train");
            let (respond_to, recv) = oneshot::channel();
            let train_cmd = types::TrainCommand::AttendeeJoin {
                respond_to,
                attendee,
            };
            transmitter
                .try_send(train_cmd)
                .context("Failed to send TrainCommand")?;
        } else {
            warn!("Failed to find Train {}", train);
            bail!("Failed to find Train {}", train);
        }
        Ok(())
    }

    fn leave(&self, train: String, attendee: String) -> Result<()> {
        info!("{} is trying to leave Train {}", attendee, train);
        if let Some(transmitter) = self.timetable.get_train_transmitter(&train) {
            debug!("Send AttendeeJoin command to Train");
            let (respond_to, recv) = oneshot::channel();
            let train_cmd = types::TrainCommand::AttendeeLeave {
                respond_to,
                attendee,
            };
            transmitter
                .try_send(train_cmd)
                .context("Failed to send TrainCommand")?;
        } else {
            warn!("Failed to find Train {}", train);
            bail!("Failed to find Train {}", train);
        }
        Ok(())
    }

    fn change_departure(&self, train: String, departure: OffsetDateTime) -> Result<()> {
        info!(
            "Trying to change departure of Train: {} to: {:?}",
            train, departure
        );
        if let Some(transmitter) = self.timetable.get_train_transmitter(&train) {
            debug!("Send AttendeeJoin command to Train");
            let (respond_to, recv) = oneshot::channel();
            let train_cmd = types::TrainCommand::ChangeDeparture {
                respond_to,
                departure,
            };
            transmitter
                .try_send(train_cmd)
                .context("Failed to send TrainCommand")?;
        } else {
            warn!("Failed to find Train {}", train);
            bail!("Failed to find Train {}", train);
        }
        Ok(())
    }
}

impl Bot for Controller {
    fn handle_message(
        &self,
        sender: String,
        msg: String,
        transmitter: mpsc::Sender<String>,
    ) -> Result<String> {
        info!("Attempting to parse message: {:?}", msg);
        match msg.parse::<types::ControllerCommand>() {
            Ok(cmd) => match cmd {
                types::ControllerCommand::Create { train, departure } => {
                    self.create_train(train, departure, transmitter)
                        .context("Failed to create the Train")?;
                }
                types::ControllerCommand::Join(train) => {
                    self.join_train(train, sender)
                        .context("Failed to join the Train")?;
                }
                types::ControllerCommand::Leave(train) => {
                    self.leave(train, sender)
                        .context("Failed to leave the Train")?;
                }
            },
            Err(err) => {
                error!("Failed to parse cmd: {:?}", err);
                bail!("Failed to parse cmd");
            }
        };
        Ok("foo".to_string())
    }
}

#[cfg(test)]
mod tests {
    // use super::*;
}
